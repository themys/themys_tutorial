
\section{Animation mode}
\label{sec:Time}

ParaView has many powerful options for controlling time and animation.  The
majority of these are accessed through the \keyterm{animation view}.  From
the menu, click on \gui{View} \ra \gui{Animation View}.

\begin{inlinefig}
  \includegraphics[width=\scw]{images/AnimationView}
\end{inlinefig}

\index{animation mode|(}

\label{AnimationMode}

We will first examine the controls at the top of the animation view. 
The \keyterm{animation mode} parameter determines how ParaView
 will step through time during playback. There are three modes available, 
however, the \gui{Real Time} mode is now deprecated and we will not focus on it.

\begin{description}
\item[Sequence] \index{sequence (animation mode)} Given a start and end
  time, break the animation into a specified number of frames spaced
  equally apart.
\item[Real Time] \index{real time (animation mode)} ParaView will play back
  the animation such that it lasts the specified number of seconds.  The
  actual number of frames created depends on the update time between
  frames. As said before, this mode is deprecated and will be remove in future versions, please don't use it.
\item[Snap To TimeSteps] \index{Snap To TimeSteps (animation mode)}
  ParaView will play back exactly those time steps that are defined by your
  data.
\end{description}

Whenever you load a file that contains time, ParaView will automatically
change the animation mode to \gui{Snap To TimeSteps}.  Thus, by default you
can load in your data, hit play~\vcrPlay, and see each time step as defined
in your data.  This is by far the most common use case.

A counter use case can occur when a simulation writes data at variable time
intervals.  Perhaps you would like the animation to play back relative to
the simulation time rather than the time index.  No problem.  We can switch
to one of the other two animation modes.  Another use case is the desire to
change the playback rate. Perhaps you would like to speed up or slow down
the animation. The other animation mode allow us to do that.

\begin{exercise}{Slowing Down an Animation}%
  \label{ex:SlowingDownAnAnimation}%
  We are going to start a fresh visualization, so if you have been following along with the exercises so far, now is a good time to reset ParaView.
  The easiest way to do this is to select \textit{reset session} in the \gui{Edit} menu or hit \resetsession from the toolbar in paraview mode.

  \begin{enumerate}
  \item Open the file \gui{can.ex2}, load all variables, \apply.
  \item Press the~\yPlus button to orient the camera to the mesh.
  \item Press the play button~\vcrPlay in the toolbars.
    \savecounter
  \end{enumerate}

  During this animation, ParaView is visiting each time step in the
  original data file exactly once.  Note the speed at which the animation
  plays.

  \begin{enumerate}
    \restorecounter
  \item If you have not done so yet, make the animation view visible:
    \gui{View} \ra \gui{Animation View}.
  \item Change the animation mode to \gui{Sequence}.  By default the
    animation is set up with the time range specified by the data and a
    generation of 10 frames.
  \item Play~\vcrPlay the animation again.
    \savecounter
  \end{enumerate}

  The result looks similar to the previous \gui{Snap To TimeSteps}
  animation, but the animation is now split in 10 frames equally distributed
  over the simulation time.

  \begin{enumerate}
    \restorecounter
  \item Change the \gui{No. Frames} to 200.
  \item Play~\vcrPlay the animation again.
  \end{enumerate}

  The animation is clearly playing back more slowly. Unless your computer
  is updating slowly, you will also notice that the animation appears
  jerkier than before.  This is because we have exceeded the temporal
  resolution of the data set.
\end{exercise}

\index{animation mode|)}

Often showing the jerky time steps from the original data is the desired
behavior; it is showing you exactly what is present in the data.  However,
if you wanted to make an animation for a presentation, you may want a
smoother animation.

There is a filter in ParaView designed for this purpose.  It is called
the \keyterm{temporal interpolator}.  This filter will interpolate the
positional and field data in between the time steps defined in the original
data set.

\begin{exercise}{Temporal Interpolation}
  \label{ex:TemporalInterpolation}%
  This exercise is a continuation of
  Exercise~\ref{ex:SlowingDownAnAnimation}. You will need to finish that
  exercise before beginning this one.

  \begin{enumerate}
  \item Make sure \gui{can.ex2} is highlighted in the pipeline browser.
  \item \filterindex{temporal interpolator} Apply the \gui{Temporal Interpolator}
 filter using the quick launch (ctrl+space Win/Linux, alt+space Mac).
  \item \apply.
  \item Split the view~\splitViewH, show the \gui{TemporalInterpolator1} in
    one, show \gui{can.ex2} in the other, and link the cameras.
  \item Play~\vcrPlay the animation.
  \end{enumerate}

  You should notice that the output from the temporal interpolator animates
  much more smoothly than the original data.
\end{exercise}

It is worth noting that the temporal interpolator can (and often does)
introduce artifacts in the data.  It is because of this that ParaView will
never apply this type of interpolation automatically; you will have to
explicitly add the \gui{Temporal Interpolator}.  In general, mesh
deformations often interpolate well but moving fields through a static mesh
do not.  Also be aware that the \gui{Temporal Interpolator} only works if
the topology remains consistent.  If you have an adaptive mesh that changes
from one time step to the next, the \gui{Temporal Interpolator} will give
errors.


\section{Text Annotation}

\sourceindex{text}
This section is a reminder about annotations, already seen in the main tutorial.
However, this thematic is linked to the creation of animations. If needed, feel free to skip it.\\
 
When using ParaView as a communication tool it is often helpful to annotate
the images you create with text.  With ParaView it is very easy to create
text annotation wherever you want in a 3D view.  There is a special
\keyterm{text source} that simply places some text in the view.

\begin{exercise}{Adding Text Annotation}
  \label{ex:AddingTextAnnotation}%
  If you are continuing this exercise after finishing
  Exercise~\ref{ex:TemporalInterpolation}, feel free to simply continue.
  If you have had to restart ParaView since or your state does not match up
  well enough, it is also fine to start with a fresh state.

  \begin{enumerate}
  \item
    Use the quick launch (ctrl+space Win/Linux, alt+space Mac) to create the \gui{Text}\filterindex{text} source and hit \apply.
  \item In the text edit box of the properties panel, type a message.
  \item Hit the \apply button.
  \end{enumerate}

  \begin{inlinefig}
    \includegraphics[width=\scw]{images/TextSource}
  \end{inlinefig}

  The text you entered appears in the 3D view.
  If you scroll down to the \gui{Display} options in the properties panel, you will see six buttons that allow you to quickly place the text in each of the four corners of the view as well as centered at the top and bottom.

  \begin{inlinefig}
    \includegraphics[width=.66\scw]{images/TextPosition}
  \end{inlinefig}

  You can place this text at an arbitrary position by clicking the \gui{Lower Left Corner} checkbox.
  With the \gui{Lower Left Corner} option checked, you can use the mouse to drag the text to any position within the view.
\end{exercise}

Often times you will need to put the current time value into the text
annotation.  Typing the correct time value can be tedious and error prone
with the standard text source and impossible when making an animation.
Therefore, there is a special \keyterm{annotate time source} that will
insert the current animation time into the string.

\sourceindex{annotate time}

\begin{exercise}{Adding Time Annotation}
  \label{ex:AddingTimeAnnotation}%
  \begin{enumerate}
  \item If you do not already have it loaded from a previous exercise, open
    the file \gui{can.ex2}, \apply.
  \item
    Add an \gui{Annotate Time} source (\gui{Sources} \ra \gui{Annotate} \ra
    \gui{Annotate Time} or use the quick launch: ctrl+space Win/Linux,
    alt+space Mac).
    \apply.
  \item Move the annotation around as necessary.
  \item Play~\vcrPlay and observe how the time annotation changes.
    \savecounter
  \end{enumerate}

  \begin{inlinefig}
    \includegraphics[width=\scw]{images/AnnotateTimeSource}
  \end{inlinefig}

  There are instances when the current animation time is not the same as
  the time step read from a data file.  Often it is important to know what
  the time stored in the data file is, and there is a special version of
  annotate time that acts as a filter.


  \begin{enumerate}
    \restorecounter
  \item Select \gui{can.ex2} in the pipeline browser.
  \item Use the quick launch (ctrl+space Win/Linux, alt+space Mac) to apply
    the \gui{Annotate Time Filter}. \filterindex{annotate time filter}
  \item \apply.
  \item Move the annotation around as necessary.
  \item Check the animation mode in the \gui{Animation View}.  If it is set
    to \gui{Snap to TimeSteps}, change it to \gui{Sequence}.
  \item Play~\vcrPlay and observe how the time annotation changes.
  \end{enumerate}

  \begin{inlinefig}
    \includegraphics[width=\scw]{images/AnnotateTimeFilter}
  \end{inlinefig}
\end{exercise}

You can close the animation view. We are done with it for now, but we will
revisit it again in Section~\ref{sec:Animations}.


\section{Save Animation}
\label{sec:SaveScreenshot}

One of the most important products of any visualization is screenshots and 
movies that can be used in presentations and reports.  In this section we 
save an animation (movie). Once again, we
will use the \gui{can.ex2} dataset.


\index{save animation|(}
\index{animation save|(}
\index{movie|(}

\begin{exercise}{Save Animation}
  \label{ex:SaveAnimation}%
This exercise could be done only in paraview mode as the \gui{save animation} button is not available with Themys.
  \begin{enumerate}
  \item Open the file \gui{can.ex2}, load all variables, and \apply.

  \item Select \gui{File} \ra \gui{Save Animation}~\icon{pqSaveAnimation24}.

  %% \begin{inlinefig}
  %%   \includegraphics[width=0.8\scw]{images/SaveAnimation1}
  %% \end{inlinefig}

  \savecounter
  \end{enumerate}


  This brings us to the file selection screen.  If you pull down the menu
  \gui{Files of type:} at the bottom of the dialog box, you will see the
  several file types including \index{Ogg/Theora}Ogg/Theora,
  \index{AVI}AVI, \index{JPEG}JPEG, and
  \index{portable~network~graphics}\index{PNG}PNG.

  Select a \gui{File name} for your file, and place it somewhere you can
  later find and delete.  \gui{AVI} will create a movie format that
  can be used on windows, and with some open source viewers.
  \index{Ogg/Theora}Ogg/Theora is used in many open source viewers.  Otherwise,
  you can create a \keyterm{flipbook}, or series of images.  These images
  can be stitched together to form a movie using numerous open source
  tools.  For now, try creating an AVI.

  \begin{enumerate}
    \restorecounter
  \item Press the \gui{OK} button.
    \savecounter
  \end{enumerate}

  The \gui{Save Animation Options} dialog box looks essentially the same as the \gui{Save Screenshot Options} dialog for screenshots.
  If you look at the advanced options \icon{pqAdvanced26}, you may note that there are some added \gui{Animation Options} that allow you to control the frame rate and narrow the time steps that will be animated.

  \begin{enumerate}
    \restorecounter
  \item Press the \gui{OK} button.
    \savecounter
  \end{enumerate}

  Using your favorite movie viewer, find and load the image you created.
\end{exercise}

\index{save animation|)}
\index{animation save|)}
\index{movie|)}



\section{Track properties}
\label{sec:Animations}

We have already seen how to animate a data set with time in it
(hit~\vcrPlay) and other ways to manipulate temporal data in
Section~\ref{sec:Time}.  However, ParaView's animation capabilities go far
beyond that.  With ParaView you can animate nearly any property of any
pipeline object.

\begin{exercise}{Animating Properties}
  \label{ex:AnimatingProperties}%
  We are going to start a fresh visualization, so if you have been following along with the exercises so far, now is a good time to reset ParaView.
  The easiest way to do this is to select \textit{reset session} in the \gui{Edit} menu or hit \resetsession from the toolbar in paraview mode.

  \begin{enumerate}
  \item
    Use the quick launch (ctrl+space Win/Linux, alt+space Mac) to create the \gui{Sphere} source and \apply it.
  \item
    If the animation view is not visible, make it so: \gui{View} \ra \gui{Animation View}.
  \item Change the \gui{No. Frames} option to 50 (10 will go far too quickly).
  \item Find the property selection widgets at the bottom of the animation
    view and select \gui{Sphere1} in the first box and \gui{Start Theta} in
    the second box.
    \begin{inlinefig}
      \includegraphics[height=2\baselineskip]{images/AddStartThetaTrack}
    \end{inlinefig}
    Hit the \icon{Plus} button.
  \end{enumerate}

  \begin{inlinefig}
    \includegraphics[width=80em]{images/BuildAnimation1}
  \end{inlinefig}

  If you play~\vcrPlay the animation, you will see the sphere open up then
  eventually wrap around itself and disappear.

  \begin{inlinefig}
    \includegraphics[width=25em]{images/AnimateSphere0}
    \includegraphics[width=25em]{images/AnimateSphere1}
    \includegraphics[width=25em]{images/AnimateSphere2}
    \includegraphics[width=25em]{images/AnimateSphere3}
  \end{inlinefig}
\end{exercise}

What you have done is created a \keyterm{track} for the \gui{Start Theta}
property of the \gui{Sphere1} object.  A track is represented as horizontal
bars in the animation view.  They hold \keyterm{key frames} that specify
values for the property at a specific time instance.  The value for the
property is interpolated between the key frames.  When you created a track
two key frames were created automatically: a key frame at the start time
with the minimal value and a key frame at the end time with the maximal
value.  The property you set here defines the start range of the sphere.

You can modify a track by double clicking on it.  That will bring up a
dialog box that you can use to add, delete, and modify key frames.

\begin{inlinefig}
  \includegraphics[width=\scw]{images/AnimationKeyframesDialog}
\end{inlinefig}

We use this feature to create a new key frame in the animation in the next
exercise.

\begin{exercise}{Modifying Animation Track Keyframes}
  \label{ex:ModifyingAnimationTrackKeyframes}%
  This exercise is a continuation of Exercise~\ref{ex:AnimatingProperties}.
  You will need to finish that exercise before beginning this one.

  \begin{enumerate}
  \item Double-click on the \gui{Sphere1 -- Start Theta} track.
  \item In the \gui{Animation Keyframes} dialog, click the \gui{New}
    button.  This will create a new key frame.
  \item Modify the first key frame value to be 360.
  \item Modify the second key frame time to be 0.5 and value to be 0.
  \item Click \gui{OK}.
  \end{enumerate}

  \begin{inlinefig}
    \includegraphics[width=80em]{images/BuildAnimation2}
  \end{inlinefig}

  When you play the animation, the sphere will first get bigger and then get
  smaller again.
\end{exercise}

You are not limited to animating just one property.  You can animate any
number of properties you wish.  Now we will create an animation that
depends on modifying two properties.

\begin{exercise}{Multiple Animation Tracks}
  \label{ex:MultipleAnimationTracks}%
  This exercise is a continuation of Exercises \ref{ex:AnimatingProperties}
  and \ref{ex:ModifyingAnimationTrackKeyframes}.  You will need to finish
  those exercises before beginning this one.

  \begin{enumerate}
  \item Double-click on the \gui{Sphere1 -- Start Theta} track.
  \item In the \gui{Animation Keyframes} dialog, \gui{Delete} the first
    track (at time step 0).
  \item Click \gui{OK}.
  \item In the animation view, create a track for the \gui{Sphere1} object,
    \gui{End Theta} property.
  \item Double-click on the \gui{Sphere1 -- End Theta} track.
  \item Change the time for the second key frame to be 0.5.
  \end{enumerate}

  \begin{inlinefig}
    \includegraphics[width=80em]{images/BuildAnimation3}
  \end{inlinefig}

  The animation will show the sphere creating and destroying itself, but this
  time the range front rotates in the same direction.  It makes for a very
  satisfying animation when you loop~\vcrLoop the animation.
\end{exercise}


\section{Moving camera}
In addition to animating properties for pipeline objects, you can animate
the camera.  ParaView provides methods for animating the camera along
curves that you specify.  The most common animation is to rotate the camera
around an object, always facing the object, and ParaView provides a means
to automatically generate such an animation.

\begin{exercise}{Camera Orbit Animations}
  \label{ex:CameraOrbitAnimations}%
  For this exercise, we will orbit the camera around whatever data you have
  loaded.  If you are continuing from the previous exercise, you are set
  up.  If not, simply load or create some data.  To see the effects, it is
  best to have asymmetry in the geometry you load.  \gui{can.ex2} is a good
  data set to load for this exercise.

  \begin{enumerate}
  \item Place the camera where you want the orbit to start.  The camera
    will move to the right around the viewpoint.
  \item Make sure the animation view panel is visible (\gui{View} \ra
    \gui{Animation View} if it is not).
  \item In the property selection widgets, select \gui{Camera} in the first
    box and \gui{Orbit} in the second box.
    \begin{inlinefig}
      \includegraphics[height=2\baselineskip]{images/AddCameraOrbit}
    \end{inlinefig}
    Hit the \icon{Plus} button.
    \savecounter
  \end{enumerate}

  \begin{inlinefig}
    \includegraphics[width=.66\scw]{images/CreateOrbitDialog}
  \end{inlinefig}

  Before the new track is created, you will be presented with a dialog box
  that specifies the parameters of the orbit.  The default values come from
  the current camera position, which is usually what you want.

  \begin{enumerate}
    \restorecounter
  \item Click \gui{OK}.
  \item Play~\vcrPlay.
  \end{enumerate}

  The camera will now animate around the object.
\end{exercise}

Another common camera annotation is to follow an object as it moves
through space. Imagine a simulation of a traveling bullet or vehicle. If we
hold the camera steady, then the object will quickly move out of view. To
help with this situation, ParaView provides a special track that allows the
camera to follow the data in the scene.

\begin{exercise}{Following Data in an Animation}%
  \label{ex:FollowingDataInAnAnimation}%
  We are going to start a fresh visualization, so if you have been following along with the exercises so far, now is a good time to reset ParaView.
  The easiest way to do this is to select \textit{reset session} in the \gui{Edit} menu or hit \resetsession from the toolbar in paraview mode.

  \begin{enumerate}
  \item Open the file \gui{can.ex2}, load all variables, \apply.
  \item Press the~\yPlus button to orient the camera to the mesh.
  \item Make sure the animation view panel is visible (\gui{View} \ra
    \gui{Animation View} if it is not).
  \item In the property selection widgets, select \gui{Camera} in the first
    box and \gui{Follow Data} in the second box.
    \begin{inlinefig}
      \includegraphics[height=2\baselineskip]{images/AddCameraFollowData}
    \end{inlinefig}
    Hit the \icon{Plus} button.
  \item Play~\vcrPlay.
  \end{enumerate}

  Note that instead of the can crushing to the bottom of the view, the
  animation shows the can lifted up to be continually centered in the
  image. This is because the camera is following the can down as it is
  crushed.
\end{exercise}
